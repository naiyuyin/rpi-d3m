# rpi_d3m_primitives_part2

The rpi_d3m_primitives python package contains the primitives developed for the DARPA d3m project by the Rensselaer Polytechnic Institute TA1 team. This repo helps install roi_d3m_primitives_part2 package. It is a extension of the original package however these primitives require the pre-installation of java environment. This repo includes two primitives: the structured classification primitive and the global causal discovery primitive.

# RPI ta1 primitives:
* STMB feature selection primitive
* S2TMB feature selection primitive
* JMI feature selection primitive
* TAN classification primitive
* **Global Causal Discovery primitive (installed with this repo)**
* **Structure Classification primitive (installed with this repo)**

# Requirement:
* JDK 8
* javabridge >= 1.0.11
`pip install javabridge==1.0.11`
* numpy
* pandas
* pg-causal [CDC py-causal](https://github.com/bd2kccd/py-causal) 
`pip install git+git://github.com/bd2kccd/py-causal`